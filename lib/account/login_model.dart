class Mlogin {
  String? result;
  dynamic token;

  Mlogin({this.result, this.token});

  Mlogin.fromJson(dynamic json) {
    result = json["result"];
    if (json["token"] != null) {
      token = json["token"];
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["result"] = result;
    if (token != null) {
      map["token"] = token;
    }
    return map;
  }
}