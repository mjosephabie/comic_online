import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'profile.dart';
import 'package:http/http.dart' as http;
import 'login_model.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isLoading = false;
  void _callLoginApi() {
    setState(() {
      isLoading = true;
    });
    var api = new LoginApi();
    api.getLoginData("javaniy", "antimage").then((value) {
      GetStorage storage = GetStorage();
      storage.write("token", value.token['session']);
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (_) => ProfilePage()),
      );
    }, onError: (error) {
      print(error.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(title: Text("Login Page")),
      body:
      Center(
        child: isLoading
            ? CircularProgressIndicator()
            : CustomCard2(
            imgPath:
            'https://gitlab.com/mjosephabie/comic_online/-/raw/50f62436879c32f97e34aac7b1f31b5468176863/assets/account_login.png',
            action: (){
              _callLoginApi();
            }),

      ),
    );
  }
}

class CustomCard2 extends StatelessWidget {
  String imgPath;
  void Function() action;

  CustomCard2(
      {required this.imgPath,
        required this.action});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(left: 15.0, right: 15.0),
        child: Container(
            height: 395.0,
            width: 225.0,
            child: Column(
              children: <Widget>[
                Stack(children: [
                  Container(height: 335.0),
                  Positioned(
                      top: 75.0,
                      child: Container(
                          padding: EdgeInsets.only(left: 15.0, right: 15.0),
                          height: 225.0,
                          width: 225.0,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25.0),
                              color: Colors.white24),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: 100.0,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    InkWell(
                                        onTap: this.action,
                                        child: Container(
                                            height: 50.0,
                                            width: 195.0,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(25.0),
                                                color: Colors.lightBlueAccent),
                                            child: Center(
                                                child: Text('Login',
                                                    style: TextStyle(
                                                        fontFamily: 'nunito',
                                                        fontSize: 14.0,
                                                        fontWeight: FontWeight.bold,
                                                        color: Colors.white)))))
                                  ],
                                )
                              ]))),
                  Positioned(
                      left: 50.0,
                      top: 25.0,
                      child: Container(
                          height: 125.0,
                          width: 125.0,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage(imgPath),
                                  fit: BoxFit.fill))))
                ]),
              ],
            )));
  }
}

class LoginApi {
  Future<Mlogin> getLoginData(String username, String pass) async {
    Map data = {
      'username': username,
      'password': pass
    };
    Uri url = Uri.parse("https://api.mangadex.org/auth/login");
    var body = json.encode(data);

    final response = await http.post(url,
        headers: {"Content-Type": "application/json"},
        body: body
    );
    final responseJson = json.decode(response.body);
    return new Mlogin.fromJson(responseJson);
  }
}