import 'package:comic_online/account/login_page.dart';
import 'package:comic_online/account/profile.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';

class ProfileNav extends StatefulWidget {
  const ProfileNav({Key? key}) : super(key: key);

  @override
  _ProfileNavState createState() => _ProfileNavState();
}

class _ProfileNavState extends State<ProfileNav> {
  GetStorage storage = GetStorage();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        // '/' : (BuildContext context) => LoginPage(),
        'profile': (BuildContext context) => ProfilePage(),
        'login': (BuildContext context) => LoginPage(),
      },
      initialRoute: (storage.read("token")?.isNotEmpty ?? false) ? 'profile' : 'login',
    );
  }
}