import 'package:comic_online/account/profile_nav.dart';
import 'package:comic_online/bottom_nav.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'login_page.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      // appBar: AppBar(title: Text("Home Page")),
      body: Center(
        child: isLoading
            ? CircularProgressIndicator()
            : CustomProfileCard(
            imgPath:
            'https://mangadex.org/avatar.png',
            username: 'Javaniy',
            userid: 'a3c0dee7-1aa7-43af-ae5a-8345fe48bc22',
            action: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) => alertLogout(context)
              );
              // setState(() {
              //   isLoading = true;
              // });
              // await GetStorage.init();
              // GetStorage storage = GetStorage();
              // storage.erase();
              // // Navigator.popUntil(context, (route) => route.isFirst);
              // Navigator.pushReplacement(
              //   context,
              //   MaterialPageRoute(builder: (_) => LoginPage()),
              // );
            }),
      ),
    );
  }

  Widget alertLogout(BuildContext context) {
    return AlertDialog(

      content: Container(
        child: Text("Are you sure you want to logout?"),
      ),
      title: Text("Warning!"),
      actions: [
        RaisedButton(
            color: Colors.lightBlueAccent,
            child: Text("Logout"),
            onPressed: () async {
              setState(() {
                isLoading = true;
              });
              await GetStorage.init();
              GetStorage storage = GetStorage();
              storage.erase();
              // Navigator.popUntil(context, (route) => route.isFirst);
              Navigator.of(context).pop();
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (_) => BottomNavBarPage(3)),
              );
            }
        ),
        RaisedButton(
            color: Colors.redAccent,
            child: Text("Cancel"),
            onPressed: () {
              Navigator.of(context).pop();
            }),
      ],
    );
  }
}

class CustomProfileCard extends StatelessWidget {
  String imgPath, username, userid;
  void Function() action;

  CustomProfileCard(
      {required this.imgPath,
        required this.username,
        required this.userid,
        required this.action});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(left: 15.0, right: 15.0),
        child: Container(
            height: 395.0,
            width: 225.0,
            child: Column(
              children: <Widget>[
                Stack(children: [
                  Container(height: 335.0),
                  Positioned(
                      top: 75.0,
                      child: Container(
                          padding: EdgeInsets.only(left: 15.0, right: 15.0),
                          height: 400.0,
                          width: 225.0,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25.0),
                              color: Colors.white24),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: 80.0,
                                ),
                                Center(
                                  child: Text(
                                    username,
                                    style: TextStyle(
                                        fontFamily: 'varela',
                                        fontSize: 32.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.blueGrey),
                                  ),
                                ),
                                Center(
                                  child: Text(
                                    'User ID : ${this.userid}',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: 'varela',
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.normal,
                                        color: Colors.blueGrey),
                                  ),
                                ),
                                SizedBox(
                                  height: 20.0,
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    InkWell(
                                        onTap: this.action,
                                        child: Container(
                                            height: 50.0,
                                            width: 195.0,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(25.0),
                                                color: Colors.lightBlueAccent),
                                            child: Center(
                                                child: Text('Logout',
                                                    style: TextStyle(
                                                        fontFamily: 'nunito',
                                                        fontSize: 14.0,
                                                        fontWeight: FontWeight.bold,
                                                        color: Colors.white)))))
                                  ],
                                )
                              ]))),
                  Positioned(
                      left: 50.0,
                      top: 25.0,
                      child: Container(
                        height: 125.0,
                        width: 125.0,
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(imgPath),
                          radius: 84,
                        ),
                      ))
                ]),
              ],
            )));
  }
}