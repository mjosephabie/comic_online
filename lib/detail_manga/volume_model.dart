// To parse this JSON data, do
//
//     final mangaVolumes = mangaVolumesFromJson(jsonString);

import 'dart:convert';

Map<String, MangaVolumes> mangaVolumesFromJson(String str) =>
    Map.from(json.decode(str)['volumes']).map(
        (k, v) => MapEntry<String, MangaVolumes>(k, MangaVolumes.fromJson(v)));

String mangaVolumesToJson(Map<String, MangaVolumes> data) => json.encode(
    Map.from(data).map((k, v) => MapEntry<String, dynamic>(k, v.toJson())));

class MangaVolumes {
  MangaVolumes({
    required this.volume,
    required this.count,
    required this.chapters,
  });

  String volume;
  int count;
  Map<String, Chapter> chapters;

  factory MangaVolumes.fromJson(Map<String, dynamic> json) => MangaVolumes(
        volume: json["volume"],
        count: json["count"],
        chapters: Map.from(json["chapters"])
            .map((k, v) => MapEntry<String, Chapter>(k, Chapter.fromJson(v))),
      );

  Map<String, dynamic> toJson() => {
        "volume": volume,
        "count": count,
        "chapters": Map.from(chapters)
            .map((k, v) => MapEntry<String, dynamic>(k, v.toJson())),
      };
}

class Chapter {
  Chapter({
    required this.chapter,
    required this.id,
    required this.count,
  });

  String chapter;
  String id;
  int count;

  factory Chapter.fromJson(Map<String, dynamic> json) => Chapter(
        chapter: json["chapter"],
        id: json["id"],
        count: json["count"],
      );

  Map<String, dynamic> toJson() => {
        "chapter": chapter,
        "id": id,
        "count": count,
      };
}
