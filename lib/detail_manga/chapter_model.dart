// To parse this JSON data, do
//
//     final chapter = chapterFromJson(jsonString);

import 'dart:convert';

List<Chapter> chapterFromJson(String str) => List<Chapter>.from(
    json.decode(str)['data'].map((x) => Chapter.fromJson(x)));

class Chapter {
  Chapter({
    required this.id,
    required this.attributes,
  });

  String id;
  Attributes attributes;

  factory Chapter.fromJson(Map<String, dynamic> json) => Chapter(
        id: json["id"],
        attributes: Attributes.fromJson(json["attributes"]),
      );
}

class Attributes {
  Attributes({
    required this.volume,
    required this.chapter,
    required this.title,
    // required this.translatedLanguage,
    required this.hash,
    required this.data,
    required this.dataSaver,
    // required this.externalUrl,
    required this.publishAt,
    required this.createdAt,
    required this.updatedAt,
    required this.version,
  });

  dynamic volume;
  String chapter;
  String title;
  // TranslatedLanguage translatedLanguage;
  String hash;
  List<dynamic> data;
  List<dynamic> dataSaver;
  // String externalUrl;
  DateTime publishAt;
  DateTime createdAt;
  DateTime updatedAt;
  int version;

  factory Attributes.fromJson(Map<String, dynamic> json) => Attributes(
        volume: json["volume"],
        chapter: json["chapter"] == null ? "" : json["chapter"],
        title: json["title"] == null ? "" : json['title'],
        // translatedLanguage: translatedLanguageValues.map[json["translatedLanguage"]],
        hash: json["hash"],
        data: List<dynamic>.from(json["data"].map((x) => x)),
        dataSaver: List<dynamic>.from(json["dataSaver"].map((x) => x)),
        // externalUrl: json["externalUrl"],
        publishAt: DateTime.parse(json["publishAt"]),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        version: json["version"],
      );
}
