import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'chapter_model.dart';
import 'package:get_storage/get_storage.dart';

Future<Chapter> fetchPages(http.Client client, String? id) async {
  HttpClient client = new HttpClient();

  client.badCertificateCallback =
      ((X509Certificate cert, String host, int port) => true);

  // ignore: close_sinks
  final request =
      await client.getUrl(Uri.parse('https://api.mangadex.org/chapter/' + id!));

  // Use the compute function to run parseMangas in a separate isolate.
  HttpClientResponse response = await request.close();

  dynamic reply = await response.transform(utf8.decoder).join();

  // print(reply);

  // Use the compute function to run parseMangas in a separate isolate.
  // return compute(parseMangas, reply);
  // print(id);
  return Chapter.fromJson(json.decode(reply)['data']);
}

class ChapterDetail extends StatelessWidget {
  ChapterDetail(
      {Key? key,
      required this.chapterid,
      required this.chapter,
      required this.title})
      : super(key: key);

  final String chapterid;
  final String chapter;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Read Ch. " + chapter + ': ' + title),
      ),
      body: FutureBuilder<Chapter>(
        future: fetchPages(http.Client(), chapterid),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Center(
              child: Text('An error has occurred!'),
            );
          } else if (snapshot.hasData) {
            return PageList(
                hash: snapshot.data!.attributes.hash,
                pages: snapshot.data!.attributes.data);
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}

class PageList extends StatelessWidget {
  const PageList({Key? key, required this.hash, required this.pages})
      : super(key: key);

  final String hash;
  final List<dynamic> pages;

  @override
  Widget build(BuildContext context) {
    GetStorage storage = GetStorage();

    // print(storage.read("token"));
    return ListView.builder(
      padding: const EdgeInsets.all(8),
      itemCount: pages.length,
      itemBuilder: (context, index) {
        // if (pages[index] == null || storage.read("token") == null)
        //   return Text('Page error');
        // else
        return Card(
            clipBehavior: Clip.antiAlias,
            child: Column(children: [
              if (pages[index] == null || storage.read("token") == null)
                Text('Page error')
              else
                Image.network('https://uploads.mangadex.org/' +
                    storage.read("token") +
                    '/data/' +
                    hash +
                    '/' +
                    pages[index])
            ]));
      },
    );
  }
}
