import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'chapter_model.dart';
import 'chapter_detail.dart';

Future<List<Chapter>> fetchChapters(http.Client client, String? id) async {
  HttpClient client = new HttpClient();

  client.badCertificateCallback =
      ((X509Certificate cert, String host, int port) => true);

  // print(reply);

  // Use the compute function to run parseMangas in a separate isolate.
  // return compute(parseMangas, reply);

  // ignore: deprecated_member_use
  List<Chapter> chapters = List<Chapter>.empty(growable: true);

  // ignore: close_sinks

  print(id);
  int offset = 0;
  int total = 9223372036854775807;
  while (offset < total) {
    print(offset.toString());
    String uri = 'https://api.mangadex.org/chapter?manga=' +
        id! +
        '&offset=' +
        offset.toString() +
        '&limit=100&translatedLanguage[]=en&order[chapter]=desc';
    // print(uri);
    final request = await client.getUrl(Uri.parse(uri));

    // Use the compute function to run parseMangas in a separate isolate.
    HttpClientResponse response = await request.close();

    dynamic reply = await response.transform(utf8.decoder).join();
    // print(id);
    // print(reply);
    chapters += chapterFromJson(reply);
    total = json.decode(reply)['total'];
    offset += 100;
  }

  // print(chapters);
  return chapters;
}

class MangaDetail extends StatelessWidget {
  MangaDetail(
      {Key? key,
      required this.id,
      required this.title,
      required this.description,
      required this.cover})
      : super(key: key);

  final String id;
  final String title;
  final String description;
  final String cover;
  // final List<MangaVolumes> volumes;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(256),
          child: AppBar(
              centerTitle: true,
              title: Text('Read ' + title + ' Manga'),
              flexibleSpace: Image(
                image: NetworkImage(
                  'https://uploads.mangadex.org/covers/' +
                      id +
                      '/' +
                      cover +
                      '.256.jpg',
                ),
                fit: BoxFit.cover,
              ),
              backgroundColor: Colors.transparent,
              bottom: PreferredSize(
                  child: Text(description.length > 150
                      ? description.substring(0, 150) + '...'
                      : description),
                  preferredSize: Size.fromHeight(128)))),
      body: FutureBuilder<List<Chapter>>(
        future: fetchChapters(http.Client(), id),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Center(
              child: Text('An error has occurred!'),
            );
          } else if (snapshot.hasData) {
            return ChapterList(chapters: snapshot.data!);
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}

class ChapterList extends StatelessWidget {
  const ChapterList({Key? key, required this.chapters}) : super(key: key);

  final List<Chapter> chapters;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(8),
      itemCount: chapters.length,
      itemBuilder: (context, index) {
        // print(chapters[index].id);
        return GestureDetector(
          onTap: () {
            if (chapters[index].attributes.data.length > 0) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ChapterDetail(
                    chapterid: chapters[index].id,
                    chapter: chapters[index].attributes.chapter,
                    title: chapters[index].attributes.title,
                  ),
                ),
              );
            }
          },
          child: Card(
              child: ListTile(
                  title: new Text("Ch. " +
                      chapters[index].attributes.chapter +
                      " : " +
                      chapters[index].attributes.title))),
        );
      },
    );
  }
}
