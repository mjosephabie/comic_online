// To parse this JSON data, do
//
//     final atHome = atHomeFromJson(jsonString);

import 'dart:convert';

AtHome atHomeFromJson(String str) => AtHome.fromJson(json.decode(str));

String atHomeToJson(AtHome data) => json.encode(data.toJson());

class AtHome {
  AtHome({
    required this.result,
    required this.baseUrl,
  });

  String result;
  String baseUrl;

  factory AtHome.fromJson(Map<String, dynamic> json) => AtHome(
        result: json["result"],
        baseUrl: json["baseUrl"],
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "baseUrl": baseUrl,
      };
}
