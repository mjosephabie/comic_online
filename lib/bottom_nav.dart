import 'package:comic_online/account/profile_nav.dart';
import 'package:comic_online/subscription/subscribtion_nav.dart';
import 'package:flutter/material.dart';
import 'package:comic_online/list_manga/home_page.dart';
import 'package:comic_online/list_manga/search_manga.dart';

class BottomNavBarPage extends StatefulWidget {
  int selectedIndex;

  BottomNavBarPage(this.selectedIndex);
  @override
  _BottomNavBarPageState createState() =>
      _BottomNavBarPageState(this.selectedIndex);
}

class _BottomNavBarPageState extends State<BottomNavBarPage> {
  List<Widget> listScreen = [
    MangaHome(),
    // Text(
    //   "Screen 2",
    //   style: TextStyle(fontSize: 30),
    // ),
    SubscriptionNav(),
    ProfileNav(),
  ];

  List<String> _listMenuPopUp = ["Pengaturan", "Keluar"];

  int currentScreen = 0;

  _BottomNavBarPageState(currentScreen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[Colors.blueGrey, Colors.lightBlueAccent])),
        ),
        elevation: 3,
        title: const Text("Baca Manga"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        // leading: Builder(builder: (BuildContext _) {
        //   // berdasarkan kondisi tertentu
        //   if (true) {
        //     return Container();
        //   }
        // }),
        actions: [
          Padding(
              padding: EdgeInsets.all(20.0),
              child: IconButton(
                icon: Icon(Icons.search),
                onPressed: () => showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text('Search Manga'),
                      content: TextField(
                        controller: _textFieldController,
                        decoration: InputDecoration(hintText: "Manga Title"),
                      ),
                      actions: <Widget>[
                        FlatButton(
                          child: Text('CANCEL'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        FlatButton(
                          child: Text('OK'),
                          onPressed: () {
                            print(_textFieldController.text);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => MangaSearch(
                                    query: _textFieldController.text),
                              ),
                            );
                          },
                        ),
                      ],
                    );
                  },
                ),
              )),
          PopupMenuButton(onSelected: (String value) {
            switch (value) {
              case 'Pengaturan':
                break;
              case 'Keluar':
                break;
            }
          }, itemBuilder: (BuildContext _) {
            return _listMenuPopUp
                .map((e) => PopupMenuItem<String>(value: e, child: Text(e)))
                .toList();
          })
        ],
      ),
      body: listScreen.elementAt(currentScreen),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          // BottomNavigationBarItem(
          //     icon: Icon(Icons.explore_outlined), label: 'Genre'),
          BottomNavigationBarItem(
              icon: Icon(Icons.book_outlined), label: 'Subscriptions'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Account'),
        ],
        currentIndex: this.currentScreen,
        selectedItemColor: Colors.lightBlueAccent,
        onTap: (int index) {
          setState(() {
            this.currentScreen = index;
          });
        },
      ),
    );
  }
}

TextEditingController _textFieldController = TextEditingController();

Future<void> _displayTextInputDialog(BuildContext context) async {
  return showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text('Search Manga'),
        content: TextField(
          controller: _textFieldController,
          decoration: InputDecoration(hintText: "Manga Title"),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('CANCEL'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          FlatButton(
            child: Text('OK'),
            onPressed: () {
              print(_textFieldController.text);
              Navigator.pop(context);
            },
          ),
        ],
      );
    },
  );
}
