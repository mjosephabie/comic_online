import 'dart:convert';
import 'package:http/http.dart' as http;

class MCover {
  String? filename;
  String? version;

  MCover({this.filename, this.version});

  MCover.fromJson(dynamic json) {
    filename = json["attributes"]["fileName"];
    version = json["attributes"]["version"].toString();
  }
}
