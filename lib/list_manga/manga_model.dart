// To parse this JSON data, do
//
//     final manga = mangaFromJson(jsonString);

import 'dart:convert';

List<Manga> mangaFromJson(String str) =>
    List<Manga>.from(json.decode(str)['data'].map((x) => Manga.fromJson(x)));

class Manga {
  final String id;
  final String title;
  final String description;
  final List<Relationship>? relationships;

  const Manga({
    required this.id,
    required this.title,
    required this.description,
    required this.relationships,
  });

  factory Manga.fromJson(Map<String, dynamic> json) {
    // var _cover;
    // for (var relationship in json['relationships']) {
    //   if (relationship['type'] == 'cover_art') {
    //     _cover = fetchCover(relationship['id']);
    //   }
    // }
    return Manga(
      id: json['id'] as String,
      title: json['attributes']['title']['en'] != null
          ? json['attributes']['title']['en']
          : json['attributes']['title']['zh'] as String,
      description: json['attributes']['description']['en'] as String,
      relationships: List<Relationship>.from(
          json["relationships"].map((x) => Relationship.fromJson(x))),
    );
  }
}

class Relationship {
  Relationship({
    required this.id,
    required this.type,
    this.attributes,
  });

  String id;
  String type;
  RelationshipAttributes? attributes;

  factory Relationship.fromJson(Map<String, dynamic> json) => Relationship(
        id: json["id"],
        type: json["type"],
        attributes: json["attributes"] == null
            ? null
            : RelationshipAttributes.fromJson(json["attributes"]),
      );
}

class RelationshipAttributes {
  RelationshipAttributes({
    required this.description,
    // required this.volume,
    required this.fileName,
    required this.createdAt,
    required this.updatedAt,
    required this.version,
  });

  String description;
  // String volume;
  String fileName;
  DateTime createdAt;
  DateTime updatedAt;
  int version;

  factory RelationshipAttributes.fromJson(Map<String, dynamic> json) =>
      RelationshipAttributes(
        description: json["description"],
        // volume: json["volume"],
        fileName: json["fileName"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        version: json["version"],
      );
}
