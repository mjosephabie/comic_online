import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'manga_model.dart';
import 'package:comic_online/detail_manga/manga_detail.dart';

Future<List<Manga>> fetchMangas(http.Client client, String query) async {
  HttpClient client = new HttpClient();

  client.badCertificateCallback =
      ((X509Certificate cert, String host, int port) => true);

  // ignore: close_sinks
  final uri = 'https://api.mangadex.org/manga?title=' +
      query +
      '&includes[]=cover_art&limit=100&availableTranslatedLanguage[]=en';

  // print(uri);
  final request = await client.getUrl(Uri.parse(uri));

  // Use the compute function to run parseMangas in a separate isolate.
  HttpClientResponse response = await request.close();

  String reply = await response.transform(utf8.decoder).join();

  // print(reply);

  // Use the compute function to run parseMangas in a separate isolate.
  // return compute(parseMangas, reply);
  return mangaFromJson(reply);
}

// List<Manga> parseMangas(String responseBody) {
//   final parsed = jsonDecode(responseBody)['data'].cast<Map<String, dynamic>>();

//   return parsed.map<Manga>((json) => Manga.fromJson(json)).toList();
// }

class MangaSearch extends StatelessWidget {
  const MangaSearch({Key? key, required this.query}) : super(key: key);

  final String query;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Search Result"),
      ),
      body: FutureBuilder<List<Manga>>(
        future: fetchMangas(http.Client(), query),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Center(
              child: Text('An error has occurred!'),
            );
          } else if (snapshot.hasData) {
            return MangasList(mangas: snapshot.data!);
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}

class MangasList extends StatelessWidget {
  const MangasList({Key? key, required this.mangas}) : super(key: key);

  final List<Manga> mangas;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(8),
      itemCount: mangas.length,
      itemBuilder: (context, index) {
        // return Image.network('https://uploads.mangadex.org/covers/' +
        //     mangas[index].id +
        //     '/' +
        //     mangas[index].cover);
        // return Text(mangas[index].title);
        var cover_art = mangas[index]
            .relationships!
            .where((element) => element.type == 'cover_art');

        var filename = cover_art.first.attributes!.fileName;
        return GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MangaDetail(
                    id: mangas[index].id,
                    title: mangas[index].title,
                    description: mangas[index].description,
                    cover: filename),
              ),
            );
          },
          child: Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: ConstrainedBox(
                      constraints: BoxConstraints(
                        minWidth: 44,
                        minHeight: 44,
                        maxWidth: 64,
                        maxHeight: 64,
                      ),
                      child: Image.network(
                          'https://uploads.mangadex.org/covers/' +
                              mangas[index].id +
                              '/' +
                              filename +
                              '.256.jpg')),
                  title: new Text(mangas[index].title),
                  // subtitle: Text(
                  //   'Secondary Text',
                  //   style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  // ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
