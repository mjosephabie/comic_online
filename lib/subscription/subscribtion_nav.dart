import 'package:comic_online/subscription/no_subs.dart';
import 'package:comic_online/subscription/subscription.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';

class SubscriptionNav extends StatefulWidget {
  const SubscriptionNav({Key? key}) : super(key: key);

  @override
  _SubscriptionNavState createState() => _SubscriptionNavState();
}

class _SubscriptionNavState extends State<SubscriptionNav> {
  GetStorage storage = GetStorage();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        // '/' : (BuildContext context) => LoginPage(),
        'subs': (BuildContext context) => SubscriptionPage(),
        'nosubs': (BuildContext context) => NoSubsPage(),
      },
      initialRoute: (storage.read("token")?.isNotEmpty ?? false) ? 'subs' : 'nosubs',
    );
  }
}