import 'dart:convert';
import 'dart:io';

import 'package:comic_online/detail_manga/manga_detail.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;

class SubscriptionPage extends StatefulWidget {
  const SubscriptionPage({Key? key}) : super(key: key);

  @override
  _SubscriptionPageState createState() => _SubscriptionPageState();
}

class _SubscriptionPageState extends State<SubscriptionPage> {
  late Future<List<MangaSubscribed>> futureManga;

  @override
  void initState() {
    super.initState();
    futureManga = fetchMSubscribed();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FutureBuilder<List<MangaSubscribed>>(
          future: fetchMSubscribed(),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return const Center(
                child: Text('An error has occurred!'),
              );
            } else if (snapshot.hasData) {
              return MangaList(manga: snapshot.data!);
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }
}

class CustomMangaCard extends StatelessWidget {
  String title, desc, imageUrl;

  CustomMangaCard(
      {required this.title, required this.desc, required this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(30.0, 5.0, 10.0, 5.0),
            height: 200.0,
            width: 300,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Padding(
              padding: EdgeInsets.fromLTRB(100.0, 20.0, 20.0, 20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: 160.0,
                        child: Text(
                          this.title,
                          style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.w600,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10.0),
                  Text(
                    this.desc,
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
//            left: 10.0,
            top: 15.0,
            bottom: 15.0,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image(
                width: 120.0,
                image: NetworkImage(
                  this.imageUrl,
                ),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class MangaList extends StatelessWidget {
  const MangaList({Key? key, required this.manga}) : super(key: key);

  final List<MangaSubscribed> manga;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      // gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
      //   crossAxisCount: 2,
      // ),
      itemCount: manga.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MangaDetail(
                    id: manga[index].id,
                    title: manga[index].title,
                    description: manga[index].desc,
                    cover: manga[index].cover),
              ),
            );
          },
          child: CustomMangaCard(
            title: manga[index].title,
            desc: manga[index].desc.substring(0, 110) + '...',
            imageUrl: 'https://uploads.mangadex.org/covers/' +
                manga[index].id +
                '/' +
                manga[index].cover +
                '.256.jpg',
          ),
        );
        // return Text(manga[index].title);
        // return Image(
        //   image: NetworkImage('https://mangadex.org/covers/${manga[index].id}/${manga[index].cover}'),
        // );
      },
    );
  }
}

class MangaSubscribed {
  String desc;
  String id;
  String title;
  String cover;

  MangaSubscribed(
      {required this.desc,
      required this.id,
      required this.title,
      required this.cover});

  factory MangaSubscribed.fromJson(Map<String, dynamic> json) {
    String tempcover = '-';
    for (int i = 0; i < json['relationships'].length; i++) {
      if (json['relationships'][i]['type'] == 'cover_art') {
        tempcover = json['relationships'][i]['attributes']['fileName'];
        break;
      }
    }
    // json['relationships'].forEach((v){
    //   if(v['type'] == 'cover_art'){
    //     tempcover = v['attributes']['fileName'];
    //     break;
    //   }
    // });
    return MangaSubscribed(
        desc: json['attributes']['description']['en'],
        id: json['id'],
        title: json['attributes']['title']['en'],
        cover: tempcover);
  }
}

List<MangaSubscribed> parseMangas(String responseBody) {
  final parsed = jsonDecode(responseBody)['data'].cast<Map<String, dynamic>>();

  return parsed
      .map<MangaSubscribed>((json) => MangaSubscribed.fromJson(json))
      .toList();
}

Future<List<MangaSubscribed>> fetchMSubscribed() async {
  GetStorage storage = GetStorage();
  final response = await http.get(
    Uri.parse(
        'https://api.mangadex.org/user/follows/manga?includes[]=cover_art'),
    headers: {
      HttpHeaders.authorizationHeader: 'Bearer ${storage.read("token")}',
    },
  );

  if (response.statusCode == 200) {
    var temp = parseMangas(response.body);
    return temp;
  } else {
    throw Exception('Failed to load album');
  }
}
