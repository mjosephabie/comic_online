import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';

class NoSubsPage extends StatefulWidget {
  const NoSubsPage({Key? key}) : super(key: key);

  @override
  _NoSubsPageState createState() => _NoSubsPageState();
}

class _NoSubsPageState extends State<NoSubsPage> {

  @override
  Widget build(BuildContext context) {
    GetStorage storage = GetStorage();
    storage.erase();
    return Scaffold(
      body: Center(
        child: Text(
          'Please Login to view subscription',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontFamily: 'varela',
              fontSize: 32.0,
              fontWeight: FontWeight.bold,
              color: Colors.black),
        ),
      ),
    );
  }
}